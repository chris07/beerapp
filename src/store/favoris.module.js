//import BeerService from '../services/beer.service';

const initialState = { favoris: null };

export const favoris = {
  namespaced: true,
  state: initialState,
  actions: {
    set({ commit }) {
      let favs =  JSON.parse(localStorage.getItem('favs'))
      commit('set', favs);
    }
  },
  mutations: {
    set(state, favoris) {
      state.favoris = favoris;
    }
  }
};
