import { createStore } from "vuex" 

import { favoris } from './favoris.module';

const store = createStore({
     modules: {favoris:favoris}
})

export default store
