import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Favoris from '../views/Favoris.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/favoris',
    name: 'Favoris',
    component: Favoris
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
