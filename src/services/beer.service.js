import axios from 'axios';

const API_URL = 'https://api.punkapi.com/v2/beers';

class BeerService {

  get(page){
    return axios.get(API_URL + '?page=' +page);
  }
  getOne(beer){
    return axios.get(API_URL + '/' + beer);
  }
}

export default new BeerService();
